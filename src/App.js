import React,{useState} from 'react';

function App() {
  const [name, setName] = useState("");
  const [textTag, setTextTag] = useState("");
  const [tagArray, setTagArray] = useState([]);
  const addTag = () =>{
    if(textTag && textTag !== ""){
      setTagArray([...tagArray,textTag])
      setTextTag("");
    }
  }
  function strReverse (str){
    const splitString = str.split("");
    const reverseArrayStr = splitString.reverse();
    const joinArrayStr = reverseArrayStr.join("");
    return joinArrayStr;
  }
  return (
    <div className="container" style={{marginTop:'40px'}}>
      <div className="row">
        <div className="col-6 offset-3">
          <input type="text" className="form-control" value={textTag} placeholder="Enter Tag" onChange={(e) => setTextTag(e.target.value)}/>
          <br/>
          <input type="submit" className="btn btn-success" onClick={addTag}/>
          <br/>
          <h4>Result</h4>
          <ul>
          {
            tagArray.map((data,key) => 
              <li key={key}>{data}</li>
            )
          }
          </ul>
        </div>
      </div>
      <br/><br/>
      <div className="row">
        <div className="col-6 offset-3">
          <input type="text" className="form-control" value={name} placeholder="Enter Tag" onChange={(e) => setName(e.target.value)}/>
          <br/>
          <br/>
          <h4>Result</h4>
          <ul>
          {
            strReverse(name)
          }
          </ul>
        </div>
      </div>
    </div>
  );
}

export default App;
